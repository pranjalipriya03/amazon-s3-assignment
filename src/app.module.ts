import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { ConnectionModule } from './connection/connection.module';
import { FileStorageModule } from './modules/file-storage/file-storage.module';
import { UsersModule } from './modules/users/users.module';
import { BucketModule } from './modules/bucket/bucket.module';


@Module({
  imports: [ConfigModule.forRoot({ isGlobal: true }), ConnectionModule, FileStorageModule, UsersModule, BucketModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
