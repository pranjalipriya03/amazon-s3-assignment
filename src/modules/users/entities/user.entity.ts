import { Entity, PrimaryGeneratedColumn, OneToMany, Column } from 'typeorm';
import { Bucket } from '../../bucket/entities/bucket.entity';


@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @OneToMany(() => Bucket, (bucket) => bucket.user)
  buckets: Bucket[];
}
