import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { User } from './entities/user.entity';
import { BucketModule } from '../bucket/bucket.module';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([User]),
  BucketModule],
  controllers: [UsersController],
  providers: [UsersService]
})
export class UsersModule {}
