import { Injectable, Res } from '@nestjs/common';
import * as fs from 'fs';
import * as path from 'path';
import * as util from 'util';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { v4 as uuidv4 } from 'uuid';
import { File } from './entities/file-storage.entity';
import { BucketService } from '../bucket/bucket.service';


const writeFileAsync = util.promisify(fs.writeFile);
const readFileAsync = util.promisify(fs.readFile);
const unlinkAsync = util.promisify(fs.unlink);

@Injectable()
export class FileStorageService {
  private readonly storagePath = path.join(
    __dirname,
    '../../../src/utils/files',
  );

  constructor(
    @InjectRepository(File)
    private readonly fileRepository: Repository<File>,
    private readonly bucketService: BucketService,
  ) {
    this.createStorageDirectory();
  }

  private createStorageDirectory() {
    if (!fs.existsSync(this.storagePath)) {
      fs.mkdirSync(this.storagePath);
    }
  }

  async getObject(bucketId: string, fileId: string) {
    console.log({ bucketId, fileId });
    const file = await this.fileRepository.findOne({
      where: { bucketId, id: fileId },
    });
    const filePath = this.getFilePath(bucketId, file.filename);
    const fileContent = await readFileAsync(filePath);
    const resultString = fileContent.toString('utf-8');
    console.log(resultString);
    return fileContent;
  }

  async putObject(bucketId: string, file: any) {
    const fileId = uuidv4();
    const filePath = this.getFilePath(bucketId, fileId);
    const fileContent = fs.readFileSync(file.path);
    await writeFileAsync(filePath, fileContent);
    const dbFile = this.fileRepository.create({
      bucketId,
      filename: fileId,
      url: `util/files/${bucketId}/${fileId}`,
    });
    const savedFile = await this.fileRepository.save(dbFile);
    return savedFile.id;
  }

  async deleteObject(bucketId: string, fileId: string) {
    const file = await this.fileRepository.findOne({
      where: { bucketId, id: fileId },
    });
    const filePath = this.getFilePath(bucketId, file.filename);
    await unlinkAsync(filePath);
    await this.fileRepository.delete({ bucketId, id: fileId });
    return 'File Deleted';
  }

  private getFilePath(bucket: string, fileId: string) {
    return path.join(this.storagePath, bucket, fileId);
  }

  async listBucketFiles(bucketId: string) {
    const bucketFiles = await this.fileRepository.find({ where: { bucketId } });
    return bucketFiles;
  }

  async listFilesInBucket(bucketId: string) {
    const files = await this.fileRepository.find({ where: { bucketId } });
    return files;
  }

  async listUserBucketsWithFiles(userId: string) {
    const buckets = await this.bucketService.listBuckets(userId, 1, 1000);
    const filesByBucket = await Promise.all(
      buckets.map(async (bucket) => {
        const files = await this.listFilesInBucket(bucket.id);
        return { bucket, files };
      }),
    );
    return filesByBucket;
  }
}
