import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Bucket } from '../../bucket/entities/bucket.entity';

@Entity()
export class File {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => Bucket, (bucket) => bucket.files)
  bucket: Bucket;

  @Column({ nullable: false }) 
  bucketId: string;

  @Column()
  filename: string;

  @Column()
  url: string;
}
