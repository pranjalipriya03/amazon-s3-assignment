import {
  Controller,
  Get,
  Post,
  Delete,
  UploadedFile,
  UseInterceptors,
  Query,
  Res,
} from '@nestjs/common';
import { FileStorageService } from './file-storage.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { multerOptions } from '../../utils/multer.options';

@Controller('file-storage')
export class FileStorageController {
  constructor(private readonly fileStorageService: FileStorageService) {}

  @Get('bucket-file')
  listBucketFiles(@Query('bucketId') bucketId: string) {
    return this.fileStorageService.listBucketFiles(bucketId);
  }

  @Get()
  getObject(
    @Query('bucketId') bucketId: string,
    @Query('fileId') fileId: string,
  ) {
    return this.fileStorageService.getObject(bucketId, fileId);
  }

  @Post()
  @UseInterceptors(FileInterceptor('file', multerOptions))
  async putObject(
    @Query('bucket') bucket: string,
    @UploadedFile() file: Express.Multer.File,
  ) {
    const fileId = await this.fileStorageService.putObject(bucket, file);
    return fileId;
  }

  @Delete()
  async deleteObject(
    @Query('bucketId') bucketId: string,
    @Query('fileId') fileId: string,
  ) {
    return await this.fileStorageService.deleteObject(bucketId, fileId);
  }

  @Get('getAll/files')
  async listUserBucketsObject(@Query('userId') userId: string) {
    const filesByBucket =
      await this.fileStorageService.listUserBucketsWithFiles(userId);
    return filesByBucket;
  }
}
