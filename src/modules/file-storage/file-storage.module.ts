import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FileStorageService } from './file-storage.service';
import { FileStorageController } from './file-storage.controller';
import { File } from './entities/file-storage.entity';
import { BucketService } from '../bucket/bucket.service';
import { Bucket } from '../bucket/entities/bucket.entity';


@Module({
  imports: [TypeOrmModule.forFeature([File]),TypeOrmModule.forFeature([Bucket])],
  controllers: [FileStorageController],
  providers: [FileStorageService,BucketService],
})
export class FileStorageModule {}
