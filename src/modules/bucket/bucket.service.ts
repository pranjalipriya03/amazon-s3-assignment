import { Injectable } from '@nestjs/common';
import { Bucket } from './entities/bucket.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class BucketService {
  constructor(
    @InjectRepository(Bucket)
    private readonly bucketRepository: Repository<Bucket>,
  ) {}

  async listBuckets(userId: string, page, pageSize) {
    const skip = (page - 1) * pageSize;
    const result = await this.bucketRepository.find({
      where: { userId },
      take: pageSize,
      skip,
    });

    return result;
  }
}
