import { Entity, PrimaryGeneratedColumn, ManyToOne, OneToMany, Column } from 'typeorm';
import { User } from '../../users/entities/user.entity';
import { File } from '../../file-storage/entities/file-storage.entity';


@Entity()
export class Bucket {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @OneToMany(() => File, (file) => file.bucket)
  files: File[];

  @ManyToOne(() => User, (user) => user.buckets)
  user: User;

  @Column() 
  userId: string;
}
