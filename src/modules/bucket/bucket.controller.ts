import { Controller, Get, Query } from '@nestjs/common';
import { BucketService } from './bucket.service';

@Controller('bucket')
export class BucketController {
    constructor(private readonly bucketService: BucketService) {}

  @Get('user')
  async listBuckets(
    @Query('userId') userId: string,
    @Query('page') page,
    @Query('pageSize') pageSize,
  ) {
    const result = await this.bucketService.listBuckets(userId, page, pageSize);
    return result;
  }
}
